import Vue from 'vue'
import Router from 'vue-router'
import EmailValidation from 'components/EmailValidation'

Vue.use(Router)

export default new Router({
  routes: [
    {path: '/', component: EmailValidation}
  ]
})
