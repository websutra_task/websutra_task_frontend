import axios from 'axios'
import resources from 'api/resources'

export const postEmailValidation = (email) => {
  return axios.post(resources.emailValidation, {email: email})
}
